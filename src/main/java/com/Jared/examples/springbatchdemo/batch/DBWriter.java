package com.Jared.examples.springbatchdemo.batch;

import com.Jared.examples.springbatchdemo.model.Book;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.Jared.examples.springbatchdemo.repository.BookRepository;

import java.util.List;

@Component
public class DBWriter implements ItemWriter<Book> {


    private BookRepository bookRepository;

    @Autowired
    public DBWriter (BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    @Override
    public void write(List<? extends Book> books) throws Exception{
        System.out.println("Data Saved for books: " + books);
        bookRepository.saveAll(books);
    }
}
