package com.Jared.examples.springbatchdemo.batch;

import com.Jared.examples.springbatchdemo.model.Book;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<Book, Book> {

    private static final Map<String, String> GENRE_NAMES =
            new HashMap<>();

    public Processor() {
        GENRE_NAMES.put("001","Horror");
        GENRE_NAMES.put("002","Fiction");
    }

    @Override
    public Book process(Book book) throws Exception {
        String genreCode = book.getGenre();
        String genre = GENRE_NAMES.get(genreCode);
        book.setGenre(genre);
        book.setTime(new Date());
        System.out.println(String.format("Converted from [%s] to [%s]", genreCode, genre));
        return book;
    }
}
