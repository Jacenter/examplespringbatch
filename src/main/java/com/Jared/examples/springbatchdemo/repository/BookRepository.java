package com.Jared.examples.springbatchdemo.repository;

import com.Jared.examples.springbatchdemo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository <Book, Integer> {
}
